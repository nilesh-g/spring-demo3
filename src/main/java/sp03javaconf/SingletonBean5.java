package sp03javaconf;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("singleton")
public class SingletonBean5 {
	// ...

	@PostConstruct
	public void init() {
		System.out.println("SingletonBean4 created.");
	}
	
	@PreDestroy
	public void destroy() {
		System.out.println("SingletonBean4 destroyed.");
	}
	
	@Lookup
	public PrototypeBean5 getInnerBean() {
		return null;
	}

	@Override
	public String toString() {
		return String.format("SingletonBean4 []");
	}
}
