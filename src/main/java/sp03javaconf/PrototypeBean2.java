package sp03javaconf;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class PrototypeBean2 {
	// ...
	
	@Autowired
	private SingletonBean2 innerBean;
	
	@PostConstruct
	public void init() {
		System.out.println("PrototypeBean2 created.");
	}
	
	@PreDestroy
	public void destroy() {
		System.out.println("PrototypeBean2 destroyed.");
	}

	public SingletonBean2 getInnerBean() {
		return innerBean;
	}

	@Override
	public String toString() {
		return String.format("PrototypeBean2 []");
	}
}
