package sp03javaconf;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class PrototypeBean4 {
	// ...
	
	@PostConstruct
	public void init() {
		System.out.println("PrototypeBean4 created.");
	}
	
	@PreDestroy
	public void destroy() {
		System.out.println("PrototypeBean4 destroyed.");
	}

	@Override
	public String toString() {
		return String.format("PrototypeBean4 []");
	}
}
