package sp03javaconf;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Sp03Main {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext ctx;
		ctx = new AnnotationConfigApplicationContext(AppConfig.class);
		//ctx.registerShutdownHook();
		/*
		Box b1 = (Box) ctx.getBean("b1");
		System.out.println("Vol 1 = " + b1.calcVolume());
		
		Box b2 = (Box) ctx.getBean("b2");
		System.out.println("Vol 2 = " + b2.calcVolume());
		*/
		
		/*
		Circle c1 = (Circle) ctx.getBean("c1");
		c1.setRadius(7);
		System.out.println("Area 1 = " + c1.calcArea());
		*/
		
		/*
		Account a1 = (Account) ctx.getBean("accountImpl");
//		a1.setAccId(1);
//		a1.setType("Saving");
//		a1.setBalance(1000.00);
//		a1.getAccHolder().setName("Steve");
//		a1.getAccHolder().setAddr("US");
//		a1.getAccHolder().setAge(65);
		System.out.println(a1);
		*/
		
		/*
		Passbook pb1 = (Passbook) ctx.getBean("passbookImpl");
		System.out.println(pb1);
		*/
		
		/*
		SingletonBean s1 = ctx.getBean(SingletonBean.class);
		SingletonBean s2 = ctx.getBean(SingletonBean.class);
		System.out.println("Singleton Beans are same : " + (s1==s2));

		PrototypeBean p1 = ctx.getBean(PrototypeBean.class);
		PrototypeBean p2 = ctx.getBean(PrototypeBean.class);
		System.out.println("Prototype Beans are same : " + (p1==p2));
		
		p1.destroy();
		p2.destroy();
		*/

		/*
		PrototypeBean2 p1 = ctx.getBean(PrototypeBean2.class);
		PrototypeBean2 p2 = ctx.getBean(PrototypeBean2.class);
		System.out.println("PrototypeBean2 are same : " + (p1==p2));
		System.out.println("Inner SingletonBean2 are same : " 
				+ (p1.getInnerBean()==p2.getInnerBean()));
		*/
	
		/*
		SingletonBean3 s1 = ctx.getBean(SingletonBean3.class);
		SingletonBean3 s2 = ctx.getBean(SingletonBean3.class);
		System.out.println("SingletonBean3 are same : " + (s1==s2));
		System.out.println("Inner PrototypeBean3 are same : " 
				+ (s1.getInnerBean()==s2.getInnerBean()));
		*/

		/*
		SingletonBean4 s1 = ctx.getBean(SingletonBean4.class);
		SingletonBean4 s2 = ctx.getBean(SingletonBean4.class);
		System.out.println("SingletonBean4 are same : " + (s1==s2));
		System.out.println("Inner PrototypeBean4 are same : " 
				+ (s1.getInnerBean()==s2.getInnerBean()));
		*/
		
		SingletonBean5 s1 = ctx.getBean(SingletonBean5.class);
		SingletonBean5 s2 = ctx.getBean(SingletonBean5.class);
		System.out.println("SingletonBean5 are same : " + (s1==s2));
		System.out.println("Inner PrototypeBean5 are same : " 
				+ (s1.getInnerBean()==s2.getInnerBean()));
		ctx.close();
	}
}
