package sp03javaconf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@PropertySource("classpath:/application.properties")
@ComponentScan//("sp03javaconf")
@Configuration
public class AppConfig {
	@Bean
	public Box b1() {
		Box b = new BoxImpl();
		b.setLength(5);
		b.setBreadth(4);
		b.setHeight(3);
		return b;
	}
	@Bean
	public Box b2() {
		Box b = new BoxImpl(8, 6, 4);
		return b;
	}
	
	@Bean
	public Account accountImpl() {
		// hard-coded values
		Account a = new AccountImpl(1, "Saving", 1000.00, null);
		return a;
	}
	
}

