package sp03javaconf;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("singleton")
public class SingletonBean4 implements ApplicationContextAware {
	// ...
	
	private ApplicationContext ctx;
	
	@Override
	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		this.ctx = ctx;
	}
	
	@PostConstruct
	public void init() {
		System.out.println("SingletonBean4 created.");
	}
	
	@PreDestroy
	public void destroy() {
		System.out.println("SingletonBean4 destroyed.");
	}
	
	public PrototypeBean4 getInnerBean() {
		return ctx.getBean(PrototypeBean4.class);
	}

	@Override
	public String toString() {
		return String.format("SingletonBean4 []");
	}
}
