package sp03javaconf;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PassbookImpl implements Passbook {
	@Value("#{accountImpl.accId}")
	private int accNo;
	@Value("#{accountImpl.getBalance()}")
	private double balance;
	//@Autowired
	@Value("#{personImpl}")
	private Person accHolderInfo;
	
	public PassbookImpl() {
	}
	
	@PostConstruct
	public void init() {
		System.out.println("PassbookImpl bean created.");
	}


	@Override
	public int getAccNo() {
		return accNo;
	}

	@Override
	public void setAccNo(int accNo) {
		this.accNo = accNo;
	}

	@Override
	public double getBalance() {
		return balance;
	}

	@Override
	public void setBalance(double balance) {
		this.balance = balance;
	}

	@Override
	public Person getAccHolderInfo() {
		return accHolderInfo;
	}

	@Override
	public void setAccHolderInfo(Person accHolderInfo) {
		this.accHolderInfo = accHolderInfo;
	}

	@Override
	public String toString() {
		return String.format("PassbookImpl [accNo=%s, balance=%s, accHolderInfo=%s]", accNo, balance, accHolderInfo);
	}

}
