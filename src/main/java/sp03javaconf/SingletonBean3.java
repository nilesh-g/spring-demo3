package sp03javaconf;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("singleton")
public class SingletonBean3 {
	// ...
	@Autowired
	private PrototypeBean3 innerBean;
		
	@PostConstruct
	public void init() {
		System.out.println("SingletonBean3 created.");
	}
	
	@PreDestroy
	public void destroy() {
		System.out.println("SingletonBean3 destroyed.");
	}
	
	public PrototypeBean3 getInnerBean() {
		return innerBean;
	}

	@Override
	public String toString() {
		return String.format("SingletonBean3 []");
	}
}
