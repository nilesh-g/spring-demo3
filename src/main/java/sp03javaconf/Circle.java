package sp03javaconf;

public interface Circle {
	void setRadius(double rad);
	double getRadius();
	double calcArea();
}
